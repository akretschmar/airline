
package airline;


public class Airline {
    private String name;
    private String shortCode;
    
    public Airline(String name, String shortCode){
        this.name = name;
        this.shortCode = shortCode;
    }
    
    public String getName(){
        return this.name;        
    }
    
    public String getShortCode(){
        return this.shortCode;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setShortCode(String shortCode){
        this.shortCode = shortCode;
    }
}
