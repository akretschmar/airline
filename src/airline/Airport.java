
package airline;

public class Airport {
    private String code;
    private String city;
    
    public Airport(String code, String city){
        this.code = code;
        this.city = city;
    }
    
    public String getCode(){
        return this.code;
    }
    
    public String getCity(){
        return this.city;
    }
    
    public void setCode(String code){
        this.code = code;
    }
    
    public void setCity(String city){
        this.city = city;
    }
}
