
package airline;

public class Airplane {
    private String id;
    private int numberOfSeats;
    private String make;
    private String model;
    
    public Airplane(String id, int numberOfSeats, String make, String model){
        this.id = id;
        this.numberOfSeats = numberOfSeats;
        this.make = make;
        this.model = model;
    }
    
    public String getId(){
        return this.id;
    }
    public int getNumberOfSeats(){
        return this.numberOfSeats;
    }
    public String getMake(){
        return this.make;
    }
    public String getModel(){
        return this.model;
    }
    
    public void setId(String id){
        this.id = id;
    }
    public void setNumberOfSeats(int numberOfSeats){
        this.numberOfSeats = numberOfSeats;
    }
    public void setMake(String make){
        this.make = make;
    }
    public void setModel(String model){
        this.model = model;
    }
    
 
}
