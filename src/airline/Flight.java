
package airline;

import java.util.Date;

public class Flight {
    
    private String code;
    private Date date;
    
    public Flight(String code, Date date){
        this.code = code;
        this.date = date;
    }
    
    public String getCode(){
    return this.code;}
    
    public Date getDate(){
    return this.date;
    }
    public void setCode(String code){
        this.code = code;
    }
    public void setDate(Date date){
        this.date = date;
    }
    
}

