
package airline;


import java.util.Date;


public class AirportSimulation {  
    public static void main(String[] args) {
                
        //Make some airlines
        Airline westJet = new Airline("West Jet", "TOR1");
        Airline airNorth = new Airline("Air North", "TO22");        
        
        //dates for flights
        Date date1 = new Date(2022, 02, 18);
        Date date2 = new Date(2022, 02, 14);
        
        //Flights for each airline
        Flight FL1 = new Flight("FL1", date1);
        Flight FL2 = new Flight("FL2", date2);
        
        
        
        //Airplane for each flight
        Airplane air1 = new Airplane("1111", 230, "Boeing", "707");
        Airplane air2 = new Airplane("2222", 280, "Boeing", "808");
        
        //All take place at this airport
        Airport airport = new Airport("Pearson International", "Toronto");
        
        System.out.println("At " + airport.getCity() + " " + airport.getCode() + ", flights from " 
                        + westJet.getName() + " " + westJet.getShortCode() + " with id: " + air1.getId()
                        + " will be arriving on " + FL1.getDate());
        
        System.out.println("Additionally, flights from " + airNorth.getName() + " " + air2.getMake() 
                           + " model " + air2.getModel() + " with id: " + air2.getId() + " will be seating " + air2.getNumberOfSeats() + " passengers on " 
                           + FL2.getDate());
        
        
        
        
        
        
        
        
    }
    
}
